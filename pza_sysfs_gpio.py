import logging
import paho.mqtt.client as mqtt
from driver_sysfs_gpio import DriverSysfsGpio
import os
import json

# Module logger
mog = logging.getLogger("pza.sysfs")

class Gpio:
    """
    """

    _bopic = ...
    ###########################################################################
    ###########################################################################

    def __init__(self, parent, cfg) -> None:
        """
        @param parent : parent server object = {
            "mqttco": mqtt connection
        }
        @param cfg  : gpio config = {
            "name"  : "name_of_gpio",   (custom name of the gpio for mqtt topic)  
            "id"    : X,                (gpio number)
            "help"  : "text to help"    (helper text to describe the gpio)
        }
        """
        # self.mqttco = parent.mqttco
        self.evloop = parent.evloop
        self.hostname = parent.cfg['host']['name']
        self.broker = parent.cfg['broker']
        self.name = cfg["name"]
        self.id = cfg["id"]
        self.help = cfg["help"]
        self.driver = DriverSysfsGpio(self.id)

        # Create the base topic
        self._bopic = os.path.join("pza", self.hostname, "pza_sysfs_gpio", self.name)

        self._cmds_table = {
        'direction': {
            'set': [self._on_cmd_direction_set, 'value'],
            'toggle': [self._on_cmd_direction_toggle, ''],
        },
        'value':{
            'set': [self._on_cmd_value_set, 'value'],
            'toggle': [self._on_cmd_value_toggle, ''],
        }
    } 


    ###########################################################################
    ###########################################################################

    def start(self):
        """
        """
        # Debug
        mog.debug("GPIO %d topic base : %s", self.id, self._bopic)

        # Mqtt connection
        self.mqttco = mqtt.Client()
        self.mqttco.on_message = self._on_message
        self.mqttco.connect(self.broker['address'], self.broker['port'], 60)

        # QUESTION: doit-on s'abonner seulement à ce qui est pris en charge ou à tout pour générer un msg d'erreur ?
        for cmd in self._cmds_table:
            for subcmd in self._cmds_table[cmd]:
                mog.debug(f"Subscribing to topic: {cmd}/{subcmd}")
                self.mqttco.subscribe(os.path.join(self._bopic, "cmds", cmd, subcmd))
        self.mqttco.loop_start()

        self._update_direction_atts()
        self._update_value_atts()
        
    ###########################################################################
    ###########################################################################

    def stop(self):
        pass

    ###########################################################################
    ###########################################################################
    
    def _on_message(self, client, userdata, msg):
        """
        """
        # POC gestion bind topic/cb
        # moulinete un peut overkill: le driver a subscribe qu'à des commandes valides. donc pas besoin de check la validité
        # TODO: checker seulement les arguments si necessaire
        mog.debug(f'{msg.topic=}')
        mog.debug(f'{msg.payload=}')
        path = msg.topic.split('/')
        if path[-2] in self._cmds_table:
            if path[-1] in self._cmds_table[path[-2]]:
                if self._cmds_table[path[-2]][path[-1]][1]:
                    payload = json.loads(msg.payload)
                    if self._cmds_table[path[-2]][path[-1]][1] in payload:
                        arg = payload[self._cmds_table[path[-2]][path[-1]][1]]
                        if type(arg) == int:
                            self._cmds_table[path[-2]][path[-1]][0](arg)
                        else:
                            mog.warning(f'bad type for arg "{self._cmds_table[path[-2]][path[-1]][1]}": expected {int} got {type(arg)}')
                    else:
                        mog.warning(f'missing payload arg "{self._cmds_table[path[-2]][path[-1]][1]}" for cmd "{path[-1]}" on "{path[-2]}". CMD NOT HANDLED!')
                else:
                    self._cmds_table[path[-2]][path[-1]][0]()
            else:
                mog.warning(f'someone tried to post cmd "{path[-1]}" on "{path[-2]}". CMD NOT HANDLED!')
        else:
            mog.warning(f'someone tried to post cmd on "{path[-2]}". CMD NOT HANDLED!')


    ###########################################################################
    ###########################################################################
    
    def _on_cmd_direction_set(self, value):
        """
        """
        mog.debug(f'_on_cmd_direction_set')
        self.driver.set_direction(value)
        self._update_direction_atts()

    ###########################################################################
    ###########################################################################

    def _on_cmd_direction_toggle(self):
        """
        """
        mog.debug(f'_on_cmd_direction_toggle')
        inverter = [1,0]
        self.driver.set_direction(inverter[self.driver.get_direction()])
        self._update_direction_atts()

    ###########################################################################
    ###########################################################################
    
    def _on_cmd_value_set(self, value):
        """
        """
        mog.debug(f'_on_cmd_value_set')
        self.driver.set_value(value)
        self._update_value_atts()

    ###########################################################################
    ###########################################################################
    
    def _on_cmd_value_toggle(self):
        """
        """
        mog.debug(f'_on_cmd_value_toggle')
        inverter = [1,0]
        self.driver.set_value(inverter[self.driver.get_value()])
        self._update_value_atts()

    ###########################################################################
    ###########################################################################

    def _update_direction_atts(self):
        mog.debug(f'_update_direction_atts')
        payload = {'value':self.driver.get_direction()}
        self.mqttco.publish(os.path.join(self._bopic, 'atts', 'direction'), json.dumps(payload))

    ###########################################################################
    ###########################################################################

    def _update_value_atts(self):
        mog.debug(f'_update_value_atts')
        payload = {'value':self.driver.get_value()}
        self.mqttco.publish(os.path.join(self._bopic, 'atts', 'value'), json.dumps(payload))