import json
import logging
import asyncio
import paho.mqtt.client as mqtt
import os

from pza_sysfs_gpio import Gpio

# Module logger
mog = logging.getLogger("pza.sysfs")

class Server:
    """
    """
    _info_path = ...
    _info_payload = {'type':'pza_sysfs_gpio', 'version': '0.1'}

    ###########################################################################
    ###########################################################################

    def __init__(self, cfg) -> None:
        """
        """
        # Server configuration
        self.cfg = cfg

        #
        self.gpios = []

        # Mqtt connection
        self.mqttco = mqtt.Client()
        # self.mqttco.on_message = self._on_message
        self.mqttco.connect(self.cfg['broker']['address'], self.cfg['broker']['port'], 60)
        #self.mqttco.subscribe(self._cmds_path)

        self._info_path = os.path.join("pza", self.cfg['host']['name'], "pza_sysfs_gpio", "info")

        # post infos
        self.mqttco.publish(self._info_path, json.dumps(self._info_payload))

        # Event loop
        self.evloop = asyncio.new_event_loop()


    ###########################################################################
    ###########################################################################

    def start(self):
        """
        """
        #
        mog.info("Start server on broker %s:%s", self.cfg['broker']['address'], self.cfg['broker']['port'])

        # 
        self.mqttco.loop_start()
        
        # Creation des handler de gpios
        for gpio in self.cfg['host']['gpios']:
            mog.info("Init GPIO %d", gpio["id"])
            gpio = Gpio(self, gpio)
            gpio.start()
            self.gpios.append(gpio)

        # Start event loop
        try:
            self.evloop.run_forever()
        finally:
            self.evloop.run_until_complete(self.evloop.shutdown_asyncgens())
            self.evloop.close()

    ###########################################################################
    ###########################################################################
    
    # def _on_message(self, client, userdata, msg):
    #     """
    #     """

    #     cmds = {
    #         "pza/all/cmd/scan": self._on_cmd_scan
    #     }

    #     mog.debug("pok")
    #     # print(client, userdata, msg)
    #     # msg.topic, str(msg.payload)
    #     print(msg.topic+" "+str(msg.payload))
        
    #     if msg.topic in cmds:
    #         print("cool")
    #         cmds[msg.topic]()
    #     else:
    #         print("not cool")

    ###########################################################################
    ###########################################################################

    # def _on_cmd_scan(self):
    #     """
    #     Manage pza/all/cmd/scan
    #     """
    #     mog.info("Scan requested")

    #     interfaces = []
    #     for gpio in self.gpios:
    #         interfaces.append({ "bopic": gpio.bopic, "type": "io" })

    #     payload = json.dumps(interfaces)

    #     self.mqttco.publish('pza/all/interfaces', payload)

