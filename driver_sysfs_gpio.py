import logging

# Module logger
mog = logging.getLogger("pza.sysfs")


class DriverSysfsGpio:
    """
    """

    def __init__(self, id) -> None:
        """
        """
        self.id = id
        self.value = 0
        self.direction = 0

    # ###########################################################################
    # ###########################################################################

    # def export(self):
    #     """
    #     """
    #     try:
    #         f = open("/sys/class/gpio/export", "w")
    #         f.write(str(self.id))
    #         f.close()
    #     except IOError as e:
    #         if e.errno == 16:
    #             mogger.warning("GPIO %s already exported", str(self.id))
    #         else:
    #             mogger.error("Error exporting GPIOs %s | %s", str(self.id), repr(e))
    #             raise Exception("Error exporting GPIOs %s | %s" % (str(self.id), repr(e)))

    # ###########################################################################
    # ###########################################################################

    # def unexport(self):
    #     pass
    #     # int gpio::disable()
    #     # {
    #     #     string exportString;
    #     #     exportString+="echo \"";
    #     #     exportString+=static_cast<ostringstream*>( &(ostringstream() << gpionum) )->str();
    #     #     exportString+="\" > /sys/class/gpio/unexport";
    #     #     system(exportString.c_str());
    #     #     return 0;
    #     # }

    # ###########################################################################
    # ###########################################################################

    def set_value(self, val):
        # try:
        #     path = "/sys/class/gpio/gpio%s/value" % self.id
        #     f = open(path, "w")
        #     f.write(str(val))
        #     f.close()
        # except IOError as e:
        #     mogger.error("Unable to set value %s to GPIO %s (%s) | %s", str(val), self.id, path, repr(e))
        self.value = val

    # ###########################################################################
    # ###########################################################################

    def get_value(self):
        """
        To get the value of the gpio
        """
        # try:
        #     f = open("/sys/class/gpio/gpio%s/value" % self.id, "r")
        #     value = f.read(1)
        #     f.close()
        #     return int(value)
        # except IOError as e:
        #     mogger.error("Unable to export get value %s", repr(e))
        mog.debug(f'{self.value=}')
        return int(self.value)

    # ###########################################################################
    # ###########################################################################

    def set_direction(self, direction):
        """
        """
        # try:
        #     f = open("/sys/class/gpio/gpio%s/direction" % self.id, "w")
        #     f.write(direction)
        #     f.close()
        # except IOError:
        #     mogger.error("Unable to export set value")
        self.direction = direction

    # ###########################################################################
    # ###########################################################################

    def get_direction(self):
        """
        """
        # try:
        #     f = open("/sys/class/gpio/gpio%s/direction" % self.id, "w")
        #     direction = f.read()
        #     f.close()
        #     return direction
        # except IOError:
        #     mogger.error("Unable to export set value")
        mog.debug(f'{self.direction=}')
        return int(self.direction)




