#!/usr/bin/python3

import json
import daemon
import logging
import argparse

from pza_sysfs_server import Server

# Module logger
mog = logging.getLogger("pza.sysfs")

#
DEFAULT_CFG_PATH='/etc/panduza/sysfs.json'

#
DEFAULT_CFG = {
    "broker": {
        "address": "localhost",
        "port": 1883
    }
}

###############################################################################
###############################################################################

def main(cfg):
    srv = Server(cfg)
    srv.start()

###############################################################################
###############################################################################

def updateConf(cfg, cfgFilePath):
    """
    """
    # Parse
    f = open(cfgFilePath)
    data = json.load(f)
    f.close()
    
    # Update 
    cfg.update(data)

    # Need validation
    # TODO

    # Debug
    mog.debug(cfg)

    # Return
    return cfg

###############################################################################
###############################################################################

if __name__ == '__main__':
    # Manage arguments
    parser = argparse.ArgumentParser(description='Manage the Panduza sysfs server')
    parser.add_argument('-c', '--cfg', dest='cfg_path', help='Path to the config file')
    parser.add_argument('-d', '--deamon', dest='start_deamon', action='store_true', help='start the plugin as a deamon')
    parser.add_argument('-l', '--log', dest='enable_logs', action='store_true', help='start the logs')
    args = parser.parse_args()

    #
    if args.enable_logs:
        logging.basicConfig(level=logging.DEBUG)

    # Manage configuration
    cfg = DEFAULT_CFG
    cfg_filepath=DEFAULT_CFG_PATH
    if(args.cfg_path):
        cfg_filepath = args.cfg_path
    cfg = updateConf(cfg, cfg_filepath)

    # Check if it must start as deamon
    if args.start_deamon:
        with daemon.DaemonContext():
            main(cfg)

    # Standart run
    else:
        main(cfg)
